FROM ubuntu:20.04
#FROM arm64v8/ubuntu:latest

ARG USERNAME=coder
# is there a way to get the id via command and set the ARG here to it? 
# (user alredy exsiting in parent image)
# docker build -t myimage \
#   --build-arg USER_ID=$(id -u) \
#   --build-arg GROUP_ID=$(id -g) .
ARG USER_UID=901
ARG USER_GID=901

ARG USERHOME=/home/${USERNAME} 

# environment settings
ENV HOME=${USERHOME}
ENV USER_UID=${USER_UID}
ENV USER_GID=${USER_GID}
ENV USERNAME=${USERNAME}

ENV DEBIAN_FRONTEND noninteractive
ENV DEBCONF_NONINTERACTIVE_SEEN true

USER root

RUN apt-get update && apt-get upgrade -y && apt install -y \
    cmake \
    curl \
    wget \
    git \
    nano \
    zsh \
    gdb \
    g++
   # cmake \
   # gdb

# DO NOT INSTALL SUDO YET!!, It gives problems with wget below
RUN \
  echo "**** install node repo ****" && \
  apt-get update && \
  apt-get install -y \
    gnupg && \
  curl -s https://deb.nodesource.com/gpgkey/nodesource.gpg.key | apt-key add - && \
  echo 'deb https://deb.nodesource.com/node_14.x focal main' \
    > /etc/apt/sources.list.d/nodesource.list && \
  echo "**** install build dependencies ****" && \
  apt-get update && \
  apt-get install -y \
    build-essential \
    libx11-dev \
    libxkbfile-dev \
    libsecret-1-dev \
    pkg-config && \
  echo "**** install runtime dependencies ****" && \
  apt-get install -y \
    jq \
    nano \
    net-tools \
    nodejs && \
  echo "**** clean up ****" && \
  apt-get purge --auto-remove -y \
    build-essential \
    libx11-dev \
    libxkbfile-dev \
    libsecret-1-dev \
    pkg-config && \
  apt-get clean && \
  rm -rf \
    /tmp/* \
    /var/lib/apt/lists/* \
    /var/tmp/*


# CREATE SYSTEM USER
RUN groupadd -r -g ${USER_GID} ${USERNAME} && \
    useradd -m -d ${USERHOME} -s /usr/bin/zsh -r -g ${USER_GID} -u ${USER_UID} ${USERNAME}

USER ${USERNAME}
# OH MY ZSH
# wget -O- writes to stdout
RUN sh -c "$(wget -O- https://github.com/deluan/zsh-in-docker/releases/download/v1.1.1/zsh-in-docker.sh)" -- \
    -t amuse

SHELL ["/bin/zsh", "-c", "source ${USERHOME}/.zshrc"]

# zinit
COPY --chown=${USER_UID}:${USER_GID} ./config/zinit ${USERHOME}/.zinit/bin
RUN mv ${USERHOME}/.zshrc ${USERHOME}/.zshrc_old
COPY --chown=${USER_UID}:${USER_GID} ./config/zinit_to_zshrc ${USERHOME}/.zshrc
#RUN chmod 744 -R ${USERNAME}/.zinit

# This parsing below did not work
#RUN echo 'source ~/.zinit/bin/zinit.zsh' >> ${USERHOME}.zshrc
#SHELL ["/bin/zsh", "-c", "source ${USERHOME}/.zshrc"] # not shown in output when docker build ..

RUN sh -c "$(source ${USERHOME}/.zshrc)"

USER root
RUN sh -c "$(ln -s  ${USERHOME}/.zinit/bin/zinit.zsh /usr/bin/zinit)"

# INSTALL sudo here not before, to avoid problem with "wget ... zsh-in-docker.sh" above
RUN apt-get install sudo

# Clean up
RUN apt-get autoremove -y \
    apt-get clean -y \
    rm -rf /var/lib/apt/lists/

# this below did not work
# RUN mkdir -p /root/.zinit/bin && \
#     cd /root/.zinit/bin/ && \
#     git clone https://github.com/zdharma/zinit.git

WORKDIR ${USERHOME}

RUN echo "root:root" | chpasswd

USER ${USERNAME}

CMD [ "/usr/bin/zsh" ]

EXPOSE 8443
