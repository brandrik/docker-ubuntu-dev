#!/usr/bin/env bash

USERNAME="";

if [ "$1" == "" ]; then
    USERNAME="coder";
else
    USERNAME=$1;
fi

echo "**********USERNAME=$USERNAME*************"

docker build --build-arg USERNAME=${USERNAME}  --no-cache=false  -t docker-ubuntu-zsh-base . 
