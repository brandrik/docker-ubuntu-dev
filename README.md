# docker-ubuntu-dev

ubuntu development environment in docker container: in future, jupyterlab, vscode via X11, ...


## BUILD

```
docker build  --build-opt USERNAME=<noname> -t docker-ubuntu-zsh-base .
```

Replace <noname> with desired user name. 



## RUN

```
docker run -it -u USERNAME  docker-ubuntu-zsh-base
```

where $USERNAME is the one you specified in the build section above.
