#!/usr/bin/env bash


docker run -it -u coder \
    --name=docker-ubuntu-zsh-base \
    -p 8443:8443 \
    -v /Users/riko/GIT/:/home/coder/GIT/ \
    --restart unless-stopped \
    docker-ubuntu-zsh-base
